
/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
const fs = require('fs')
function readLipsum() {
    return new Promise((resolve, reject) => {
        fs.readFile('./lipsum.txt', 'utf8', (err, data) => {
            if (!err) {
                resolve(data)
            } else {
                reject('File not read')
            }

        })

    })
}
function newFileUpperCase(data) {
    return new Promise((resolve, reject) => {
        let uppercase = data.toUpperCase()
        fs.writeFile('./uppercase.txt', uppercase, (err) => {
            if (!err) {
             console.log(uppercase)
            } else {
               console.error(err)
            }
        })
        fs.writeFile('./filename.txt', ' uppercase.txt', (err) => {
            if (!err) {
                resolve('save the new file name1')
            } else {
                reject('failed to save file name')
            }
        })
    })
}
function readNewFile(data) {
    return new Promise((resolve, reject) => {
        fs.readFile('./uppercase.txt', 'utf8', (err, data) => {
            if (err) {
               console.error(err)
            }
           
        })
        data = data.toLowerCase();

        let lowercase = data.split();
        lowercase = JSON.stringify(lowercase)
        // console.log(lowercase)
        fs.writeFile('lowercase.txt', lowercase, (err) => {
            if (err) {
                console.error(err)
            } 
        })
        fs.appendFile('./filename.txt', ' lowercase.txt', (err) => {
            if (!err) {
                resolve('save the new file name2')
            } else {
                reject('failed to save file name')
            }
        })

    })
}
function sortFile(data) {
    return new Promise((resolve, reject) => {
        // console.log(data)
        fs.readFile('./lowercase.txt', 'utf8', (err, data) => {
            if (err) {
                console.error(err)
             }
        })
        let arrData = data.split("");
        arrData.sort();

        let arrData1 = JSON.stringify(arrData)
        fs.writeFile('sorted.txt', arrData1, (err) => {
            if (err) {
                console.error(err)
             }
        })
        fs.appendFile('./filename.txt', ' sorted.txt', (err) => {
            if (!err) {
                resolve('save the new file name2')
            } else {
                reject('failed to save file name')
            }
        })



    })
}
function deleteAllFiles(data) {
   
    return new Promise((resolve, reject) => {
        fs.readFile('./lowercase.txt', 'utf8', (err, data) => {
            if (err) {
                reject(err)
            }
        
             data = data.split("");
            for (let i = 0; i < data.length; i++) {
            
                fs.unlink(`${data[i]}`, (err) => {
                    if (!err){
                        resolve()
                    }else{
                        reject()
                    }

                    //console.log(`${arr[i]} deleted successfully`)
                })
            }
        })
    })
}
readLipsum()
    .then((data) => {
        console.log('File read sucessfully')
        return newFileUpperCase(data)
    }).then((data) => {

        console.log('write data to upper case sucessfully')
        return readNewFile(data)

    }).then((data) => {

        console.log('write data to lower case sucessfully')
        return sortFile(data)

    }).then((data) => {

        console.log('sorting sucessfully')
         return deleteAllFiles(data)

    }).then((data) => {

        console.log('Deleted Sucessfully')
        //  return deleteAllFiles(data)

    }).catch((data) => {
        console.log(data)
    })
