const fs = require('fs')
function createDirectory() {
    return new Promise((resolve, reject) => {
        fs.mkdir('newFolder', function (err) {
            if (!err) {
                resolve('Folder  created')
            }
            else {
                reject("Failed")
            }
        })

    })

}
function createFiles() {
    return new Promise((resolve, reject) => {
        for (let i = 1; i <= 2; i++) {
            fs.writeFile(`./newFolder/file${i}.json`, 'Simply Easy Learning!', function (err) {
                if (!err) {
                    resolve("files created")
                } else {
                    reject("Files not created")
                }
            })
        }

    })
}
function deleFiles() {
    return new Promise((resolve, reject) => {
        for (let i = 1; i <= 2; i++) {
            fs.unlink(`./newFolder/file${i}.json`, function (err) {
                if (!err) {
                    resolve("files deleted")
                } else {
                    reject("Files not deleted")
                }
            });
        }
    })


}
createDirectory()
.then((data) => {
    console.log(data)
    return createFiles()
}).then((data) => {
    console.log(data)
    return deleFiles()
}).then((data)=>{
    console.log(data)
})
.catch((data) => {
    console.log(data)
})
