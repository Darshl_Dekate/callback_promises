const fs = require('fs')


function writeFile(filename, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile( filename, data, (err) => {
            if (!err) {
                resolve(data)
            } else {
                reject(err)
            }
        })
    })
}
function readFile(filename) {
    return new Promise((resolve, reject) => {
        fs.readFile(filename, 'utf-8', (err, data) => {
            if (!err) {
                resolve(data)
            }
            else {
                reject(err)
            }
        })
    })
}

function appendFile(filename) {
    return new Promise((resolve, reject) => {
        
        fs.appendFile('./filename1.txt', filename, (err) => {
            if (!err) {
                resolve( filename)
            } else {
                reject(err)
            }
        })
    })

}
function deleteFile(data) {
    return new Promise((resolve, reject) => {
        let data1 = data.split(" ");
        console.log(data1)
        for (let i = 0; i < data1.length; i++) 
        fs.unlink(`${data1[i]}`, (err) => {
            if(!err){
                resolve("deleted")
            }else{
                reject("Failed to delete")
            }



        })
    })

}

const promise = readFile('./lipsum.txt')

promise
    .then((data) => {
        // console.log(data)
        let uppercase = data.toUpperCase()
        return writeFile('file1.txt', uppercase)
    })
    .then((file) => {
        // console.log(file)
        return appendFile('file1.txt')
    })
    .then((file) => {
        // console.log(file)
        return readFile(file)
    })
    .then((data) => {
        let lowercase = data.toLowerCase()
        let splitLower = lowercase.split("\n");
        let lowercase1 = JSON.stringify(splitLower)
        return writeFile('file2.txt', lowercase1)
    })
    .then((filename) => {
        // console.log(filename)
        
        return appendFile(' file2.txt')
    })
    .then((readData) => {
        // console.log(readData)
        let trimFile=readData.trim()
       // console.log(trimFile)
        return readFile(trimFile)

    }).then((data) => {
       // console.log(data)
        let arrData = data.split("");
        arrData.sort();
        let arrData1 = JSON.stringify(arrData)
        return writeFile('fileSorted.txt', arrData1)
    }).then((filename) => {
        // console.log(filename)
        return appendFile(' fileSorted.txt')
    }).then(()=>{
       // let trimFile=data.trim()
       // console.log(filename1.txt)
       return readFile('filename1.txt')

    }).then((data)=>{
       // console.log(data)
       return deleteFile(data)
    })
    .then((data)=>{
        console.log(data)
    })

    .catch((err) => {
        console.log(err)
    })



